#!/usr/bin/gnuplot -persist
set terminal pngcairo  enhanced font "arial,10" fontscale 0.75 size 600, 400
set output 'median.png'
set style fill   solid 1.00 border lt -1
set key fixed right top vertical Right noreverse noenhanced autotitle nobox
set style histogram clustered gap 5 title textcolor lt -1
set style data histograms
set xtics border in scale 0,0 nomirror rotate by -45  autojustify
set title "Median time to sort an array of 8192 elements"
# set the y range so that the legend and the bar doesn't collide 
set yrange [ 0.00000 : 58500. ] noreverse writeback
set key autotitle columnheader
plot 'median.dat' using 2:xtic(1) ti col, '' u 3 ti col, '' u 4 ti col, '' u 5 ti col, '' u 6 ti col, '' u 7 ti col, '' u 8 ti col, '' u 9 ti col
