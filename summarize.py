# copy paste the output you find in openocd into a file named /tmp/foo.txt

import statistics


def summarize(s):
    (name, s) = s.split(": ")
    values = list(map(int, s.split(",")[:-1]))
    minimum = min(values)
    maximum = max(values)
    avg = sum(values) / len(values)
    med = statistics.median(values)
    print("|", name, "|", minimum, "|", maximum, "|", avg, "|", med, "|")


for line in open("/tmp/foo.txt").read().split("\n"):
    summarize(line)
