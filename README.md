Adapted from https://github.com/rust-embedded/cortex-m-quickstart

# Running the benchmarks

Connect the Nucleo-64 with an STM32L476 to the computer. In a terminal, run `openocd` in the root directory of the repository. In another terminal, run `cargo run --release`. In the gdb shell that opened, run continue two times. In the terminal where you launched `openocd`, the result should appears.

# Results

The unit is in count of the timer. I don't know to what that correspond in time. 

## random:
|algorithm | minimum | maximum | average | median |
|----------|---------|---------|---------|--------|
| glidesort::sort | 403 | 39803 | 26855.81 | 30660.5 |
| indexsort::sort_slice | 100 | 39763 | 12781.27 | 11534.0 |
| indexsort::sort_slice_stable | 784 | 39500 | 18254.51 | 16052.5 |
| ipn::sort | 551 | 23087 | 16183.39 | 16255.0 |
| ipn_unstable::sort | 295 | 39979 | 9915.74 | 7814.0 |
| v.sort() | 11687 | 15892 | 13444.8 | 13330.5 |
| v.sort_unstable() | 128 | 39448 | 25268.52 | 30064.0 |
| wpwoodjr::sort | 20 | 39972 | 33674.43 | 38550.0 |


## sorted:
|algorithm | minimum | maximum | average | median |
|----------|---------|---------|---------|--------|
| glidesort::sort | 30227 | 30227 | 30227.0 | 30227.0 |
| indexsort::sort_slice | 18607 | 18607 | 18607.0 | 18607.0 |
| indexsort::sort_slice_stable | 11025 | 11026 | 11025.52 | 11026.0 |
| ipn::sort | 38657 | 38660 | 38659.49 | 38660.0 |
| ipn_unstable::sort | 33086 | 33087 | 33086.48 | 33086.0 |
| v.sort() | 36317 | 36320 | 36319.49 | 36320.0 |
| v.sort_unstable() | 30927 | 30927 | 30927.0 | 30927.0 |
|  wpwoodjr::sort | 37169 | 37171 | 37170.98 | 37171.0 |


## random between 0 and 1:
|algorithm | minimum | maximum | average | median |
|----------|---------|---------|---------|--------|
| glidesort::sort | 23625 | 39731 | 25437.56 | 25308.0 |
| indexsort::sort_slice | 3320 | 37218 | 9113.35 | 6596.5 |
| indexsort::sort_slice_stable | 165 | 39621 | 18114.37 | 18336.0 |
| ipn::sort | 2887 | 34484 | 23823.2 | 28739.0 |
| ipn_unstable::sort | 1307 | 27526 | 5665.37 | 3017.5 |
| v.sort() | 702 | 39732 | 18326.76 | 13105.5 |
| v.sort_unstable() | 75 | 39994 | 28918.34 | 38686.0 |
| wpwoodjr::sort | 17882 | 28558 | 22912.66 | 22705.0 |


## random between 0 and 9:
|algorithm | minimum | maximum | average | median |
|----------|---------|---------|---------|--------|
| glidesort::sort | 12109 | 37694 | 23355.57 | 22757.5 |
| indexsort::sort_slice | 52 | 39741 | 17275.96 | 11953.5 |
| indexsort::sort_slice_stable | 507 | 38987 | 19681.64 | 18985.5 |
| ipn::sort | 398 | 39601 | 18459.19 | 16448.5 |
| ipn_unstable::sort | 5285 | 35736 | 14137.99 | 13457.0 |
| v.sort() | 30676 | 37206 | 33218.86 | 33220.5 |
| v.sort_unstable() | 75 | 39541 | 16573.3 | 15992.5 |
| wpwoodjr::sort | 14991 | 20951 | 18018.23 | 18081.5 |


## sorted  between 0 and 9:
|algorithm | minimum | maximum | average | median |
|----------|---------|---------|---------|--------|
| glidesort::sort | 30224 | 30226 | 30225.55 | 30226.0 |
| indexsort::sort_slice | 6304 | 6304 | 6304.0 | 6304.0 |
| indexsort::sort_slice_stable | 11026 | 11028 | 11026.54 | 11027.0 |
| ipn::sort | 12394 | 12396 | 12395.98 | 12396.0 |
| ipn_unstable::sort | 33084 | 33086 | 33085.47 | 33085.0 |
| v.sort() | 36318 | 36321 | 36320.48 | 36320.5 |
| v.sort_unstable() | 30927 | 30929 | 30927.46 | 30927.0 |
| wpwoodjr::sort | 37170 | 37172 | 37171.98 | 37172.0 |


## descending
|algorithm | minimum | maximum | average | median |
|----------|---------|---------|---------|--------|
| glidesort::sort | 6423 | 6423 | 6423.0 | 6423.0 |
| indexsort::sort_slice | 2234 | 2235 | 2234.51 | 2235.0 |
| indexsort::sort_slice_stable | 31538 | 31538 | 31538.0 | 31538.0 |
| ipn::sort | 3477 | 3477 | 3477.0 | 3477.0 |
| ipn_unstable::sort | 3653 | 3654 | 3653.99 | 3654.0 |
| v.sort() | 16483 | 16485 | 16484.98 | 16485.0 |
| v.sort_unstable() | 1629 | 1630 | 1629.47 | 1629.0 |
| wpwoodjr::sort | 3149 | 3150 | 3149.58 | 3150.0 |


![Histograms of the median times](median.png)

# License

This template is licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)

- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

## Code of Conduct

Contribution to this crate is organized under the terms of the [Rust Code of
Conduct][CoC], the maintainer of this crate, the [Cortex-M team][team], promises
to intervene to uphold that code of conduct.

[CoC]: https://www.rust-lang.org/policies/code-of-conduct
[team]: https://github.com/rust-embedded/wg#the-cortex-m-team
