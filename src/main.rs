#![no_std]
#![no_main]
#![feature(
    alloc_error_handler,
    maybe_uninit_slice,
    ptr_sub_ptr,
    sized_type_properties,
    specialization,
    strict_provenance
)]

extern crate alloc;

mod ipn;
mod ipn_unstable;
mod wpwoodjr;

use core::{alloc::Layout, hint::black_box};

use alloc::vec::Vec;

use cortex_m::asm;
use cortex_m_semihosting::{hprint, hprintln};
use embedded_alloc::Heap;
//use cortex_m_semihosting::hprintln;
// pick a panicking behavior
use panic_halt as _; // you can put a breakpoint on `rust_begin_unwind` to catch panics
                     // use panic_abort as _; // requires nightly
                     // use panic_itm as _; // logs messages over ITM; requires ITM support
                     // use panic_semihosting as _; // logs messages to the host stderr; requires a debugger

use cortex_m_rt::entry;

use stm32_hal2::{
    clocks::{Clk48Src, Clocks, InputSrc, PllCfg, PllSrc, Pllm, Pllp, Pllr},
    pac::{self, TIM3},
    rng::Rng,
    timer::{Timer, TimerConfig},
};

#[global_allocator]
static HEAP: Heap = Heap::empty();

#[alloc_error_handler]
fn oom(_: Layout) -> ! {
    hprintln!("oom");
    asm::bkpt();

    loop {}
}

fn read_rng(rng: &mut Rng) -> i32 {
    // In a real word use case, we should check for error and include a
    // timeout and return a Result. But for this use case, we prefer a more
    // simple approach, and we will see if there is an failure and manually
    // kill the program.
    while !rng.reading_ready() {}
    rng.read()
}

fn single_benchmark<A, S>(
    benchmark_name: &str,
    algo_name: &str,
    mut v: &mut Vec<i32>,
    timer: &mut Timer<TIM3>,
    mut setup: S,
    algo: A,
) where
    A: Fn(&mut [i32]),
    S: FnMut(&mut Vec<i32>),
{
    hprint!("{}, {}: ", benchmark_name, algo_name);
    for _ in 0..100 {
        setup(v);
        timer.reset_count(); // Reset the count to 0.
        algo(black_box(v.as_mut_slice()));
        let elapsed = timer.read_count();
        hprint!("{}, ", elapsed);
    }
    hprintln!("");
}

fn benchmarks<F>(name: &str, v: &mut Vec<i32>, timer: &mut Timer<TIM3>, rng: &mut Rng, f: F)
where
    F: Fn(&mut [i32]),
{
    single_benchmark(
        "random",
        name,
        v,
        timer,
        |v| {
            for elem in v.iter_mut() {
                *elem = read_rng(rng);
            }
        },
        &f,
    );

    single_benchmark("sorted", name, v, timer, |_| {}, &f);
    single_benchmark(
        "random between 0 and 1",
        name,
        v,
        timer,
        |v| {
            for elem in v.iter_mut() {
                *elem = read_rng(rng) % 2;
            }
        },
        &f,
    );
    single_benchmark(
        "random between 0 and 9",
        name,
        v,
        timer,
        |v| {
            for elem in v.iter_mut() {
                *elem = read_rng(rng) % 10;
            }
        },
        &f,
    );
    single_benchmark("sorted between 0 and 9", name, v, timer, |_| {}, &f);
    single_benchmark(
        "descending",
        name,
        v,
        timer,
        |v| {
            for i in 0..v.len() {
                v[i] = (v.len() - i) as i32;
            }
        },
        &f,
    );
}

#[entry]
fn main() -> ! {
    // Initialize the allocator BEFORE you use it
    {
        use core::mem::MaybeUninit;
        const HEAP_SIZE: usize = 92160;
        static mut HEAP_MEM: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
        unsafe { HEAP.init(HEAP_MEM.as_ptr() as usize, HEAP_SIZE) }
    }
    let mut v = Vec::with_capacity(8192);

    // Set up CPU peripherals
    let mut _cp = cortex_m::Peripherals::take().unwrap();
    // Set up microcontroller peripherals
    let dp = pac::Peripherals::take().unwrap();

    let clock_cfg = Clocks {
        input_src: InputSrc::Pll(PllSrc::Hsi),
        pll: PllCfg {
            enabled: true,
            pllp_en: true,
            divm: Pllm::Div1,
            divn: 10,
            divp: Pllp::Div7,
            divq: Pllr::Div2,
            divr: Pllr::Div2,
            ..PllCfg::disabled()
        },
        clk48_src: Clk48Src::Msi,
        ..Default::default()
    };
    clock_cfg.setup().unwrap();
    clock_cfg.enable_msi_48();

    let mut rng = Rng::new(dp.RNG);

    let timer_config = TimerConfig {
        ..Default::default()
    };

    for _ in 0..8192 {
        //hprint!("{}, ", i);
        while !rng.reading_ready() {}
        v.push(rng.read());
    }

    let mut timer = Timer::new_tim3(dp.TIM3, 1000., timer_config, &clock_cfg);

    timer.enable(); // Start the counter.

    benchmarks("v.sort_unstable()", &mut v, &mut timer, &mut rng, |v| {
        v.sort_unstable()
    });
    benchmarks("v.sort()", &mut v, &mut timer, &mut rng, |v| v.sort());
    benchmarks("ipn::sort", &mut v, &mut timer, &mut rng, ipn::sort);
    benchmarks(
        "ipn_unstable::sort",
        &mut v,
        &mut timer,
        &mut rng,
        ipn_unstable::sort,
    );
    benchmarks(
        "wpwoodjr::sort",
        &mut v,
        &mut timer,
        &mut rng,
        wpwoodjr::sort,
    );

    benchmarks("indexsort::sort_slice", &mut v, &mut timer, &mut rng, |v| {
        indexsort::sort_slice(v, |v, a, b| v[a] < v[b])
    });

    benchmarks(
        "indexsort::sort_slice_stable",
        &mut v,
        &mut timer,
        &mut rng,
        |v| indexsort::sort_slice_stable(v, |v, a, b| v[a] < v[b]),
    );

    benchmarks("glidesort::sort", &mut v, &mut timer, &mut rng, |v| {
        glidesort::sort(v)
    });

    loop {}
}
